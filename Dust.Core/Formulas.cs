﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dust.Core
{
    public static class Formulas
    {
        public static double PB { get; set; }  //B2
        public static double VNORM { get; set; }  //B3
        public static double XG { get; set; }  //B4
        public static double PG { get; set; }  //B5
        public static double XCO { get; set; }  //B6
        public static double XCO2 { get; set; }  //B7
        public static double XO2 { get; set; }  //B8
        public static double XN2 { get; set; }  //B9
        public static double XSO2 { get; set; }  //B10
        public static double XNO2 { get; set; }  //B11
        public static double XH2S { get; set; }  //B12
        public static double XH2O { get; set; }  //B13
        public static double TG { get; set; }  //B14
        public static double ZG { get; set; }  //B15
        public static double N { get; set; }  //B16
        public static double DI { get; set; }  //B17
        public static double DPART { get; set; }  //B18
        public static double CLG { get; set; }  //B19
        public static double WOPT { get; set; }  //B23
        public static double DM { get; set; }  //B24
        public static double NC { get; set; }  //B25
        public static double KGC { get; set; }  //B27
        public static double K1 { get; set; }  //B28
        public static double K2 { get; set; }  //B29
        public static double K3 { get; set; }  //B30
        public static double DSD { get; set; }  //B41
        //------------------------------------------
        public static double M_COD { get; set; } //H6
        public static double M_CO2D { get; set; } //H7
        public static double M_O2D { get; set; } //H8
        public static double M_N2D { get; set; } //H9
        public static double M_SO2D { get; set; } //H10
        public static double M_NO2D { get; set; } //H11
        public static double M_H2SD { get; set; } //H12
        public static double M_H2OD { get; set; } //H13
        public static double KD { get; set; }  //E5
        //Переменные для записи результата рассчетов
        public static double BETAD { get; set; }  //B33
        public static double DNORMD { get; set; }  //B34
        public static double DRABD { get; set; }  //B35
        public static double WDRABD { get; set; }  //B36
        public static double DINVZD { get; set; }  //B37
        public static double VGD { get; set; }  //B38
        public static double SCIKD { get; set; }  //B39
        public static double DCD { get; set; }  //B40
        public static double WGD { get; set; }  //B42
        public static double DWD { get; set; }  //B43
        public static double KSICD { get; set; }  //B44
        public static double DPD { get; set; }  //B45
        public static double KPD { get; set; }  //B46
        public static double DMIND { get; set; }  //B47

        /// <summary>
        /// Поправочный коэффициент BETA
        /// </summary>
        /// <param name="pb">барометрическое давление</param>
        /// <param name="pg">избыточное давление (+) или разрежение (-) очищаемого газа</param>
        /// <param name="tg">температура очищаемого газа</param>
        /// <returns></returns>
        public static double BETA(double pb, double pg, double tg)
        {
            return Math.Round(273 * (pb + pg) / (101.1 * (tg + 273)), 3);
        }

        /// <summary>
        /// Плотность сухого газа при нормальных условиях
        /// </summary>
        /// <param name="xco">состав газа по компонентам Х(CO)</param>
        /// <param name="xco2">состав газа по компонентам Х(CO2)</param>
        /// <param name="xo2">состав газа по компонентам  Х(О2)</param>
        /// <param name="xn2">состав газа по компонентам Х(N2)</param>
        /// <param name="xso2">состав газа по компонентам Х(SO2)</param>
        /// <param name="xno2">состав газа по компонентам Х(NO2)</param>
        /// <param name="xh2s">состав газа по компонентам Х(H2S)</param>
        /// <returns></returns>
        public static double DNORM(double xco, double xco2, double xo2, double xn2, double xso2, double xno2, double xh2s)
        {
            return Math.Round(1.97 * xco2 + 1.25 * xn2 + 1.539 * xh2s + 1.25 * xco + 1.429 * xo2 + 2.927 * xso2 + 2.051 * xno2, 3);
        }

        /// <summary>
        /// Плотность сухого газа при рабочих условиях
        /// </summary>
        /// <param name="beta">Поправочный коэффициент BETA</param>
        /// <param name="dnorm">Плотность сухого газа при нормальных условиях</param>
        /// <returns></returns>
        public static double DRAB(double beta, double dnorm)
        {
            return Math.Round(beta * dnorm, 3);
        }

        /// <summary>
        /// Плотность влажного газа при рабочих условиях
        /// </summary>
        /// <param name="beta">Поправочный коэффициент BETA</param>
        /// <param name="dnorm">Плотность сухого газа при нормальных условиях</param>
        /// <returns></returns>
        public static double WDRAB(double xg, double beta, double dnorm)
        {
            return Math.Round((dnorm + xg) * beta / (1 + (xg / 0.804)), 2);
        }

        /// <summary>
        /// Коэффициент для пересчета сухого газа на состав влажного газа
        /// </summary>
        /// <param name="xg">влажность очищаемого газа</param>
        /// <returns></returns>
        public static double K(double xg)
        {
            return Math.Round(100 / (100 + (124.4 * xg)), 3);
        }

        /// <summary>
        /// Пересчет сухого газа на состав влажного газа
        /// </summary>
        /// <param name="tg">температура очищаемого газа</param>
        /// <returns></returns>
        public static double M_CO(double tg)
        {
            return Math.Round(16.6 * (Math.Pow(10, -6)) * (373 / (tg + 273 + 100)) * Math.Pow((tg + 273) / 273, 1.5), 8);
        }

        /// <summary>
        /// Пересчет сухого газа на состав влажного газа
        /// </summary>
        /// <param name="tg">температура очищаемого газа</param>
        /// <returns></returns>
        public static double M_CO2(double tg)
        {
            return Math.Round(14.96 * (Math.Pow(10, -6)) * (528 / (tg + 273 + 255)) * Math.Pow((tg + 273) / 273, 1.5), 8);
        }

        /// <summary>
        /// Пересчет сухого газа на состав влажного газа
        /// </summary>
        /// <param name="tg">температура очищаемого газа</param>
        /// <returns></returns>
        public static double M_O2(double tg)
        {
            return Math.Round(20.396 * (Math.Pow(10, -6)) * (404 / (tg + 273 + 131)) * Math.Pow((tg + 273) / 273, 1.5), 8);
        }

        /// <summary>
        /// Пересчет сухого газа на состав влажного газа
        /// </summary>
        /// <param name="tg">температура очищаемого газа</param>
        /// <returns></returns>
        public static double M_N2(double tg)
        {
            return Math.Round(17 * (Math.Pow(10, -6)) * (387 / (tg + 273 + 114)) * Math.Pow((tg + 273) / 273, 1.5), 8);
        }

        /// <summary>
        /// Пересчет сухого газа на состав влажного газа
        /// </summary>
        /// <param name="tg">температура очищаемого газа</param>
        /// <returns></returns>
        public static double M_SO2(double tg)
        {
            return Math.Round(11.7 * (Math.Pow(10, -6)) * (669 / (tg + 273 + 396)) * Math.Pow((tg + 273) / 273, 1.5), 8);
        }

        /// <summary>
        /// Пересчет сухого газа на состав влажного газа
        /// </summary>
        /// <param name="tg">температура очищаемого газа</param>
        /// <returns></returns>
        public static double M_NO2(double tg)
        {
            return Math.Round(28.8 * (Math.Pow(10, -6)) * (464 / (tg + 273 + 191)) * Math.Pow((tg + 273) / 273, 1.5), 8);
        }

        /// <summary>
        /// Пересчет сухого газа на состав влажного газа
        /// </summary>
        /// <param name="tg">температура очищаемого газа</param>
        /// <returns></returns>
        public static double M_H2S(double tg)
        {
            return Math.Round(11.6 * (Math.Pow(10, -6)) * (773 / (tg + 273 + 191)) * Math.Pow((tg + 273) / 273, 1.5), 8);
        }

        /// <summary>
        /// Пересчет сухого газа на состав влажного газа
        /// </summary>
        /// <param name="tg">температура очищаемого газа</param>
        /// <returns></returns>
        public static double M_H2O(double tg)
        {
            return Math.Round((Math.Pow(10, -6)) * (1234 / (tg + 273 + 961)) * Math.Pow((tg + 273) / 273, 1.5), 9);
        }

        /// <summary>
        /// Динамическая вязкость газа при рабочих условиях
        /// </summary>
        /// <param name="k">Коэффициент для пересчета сухого газа на состав влажного газа</param>
        /// <param name="xco">состав газа по компонентам Х(CO)</param>
        /// <param name="xco2">состав газа по компонентам Х(CO2)</param>
        /// <param name="xo2">состав газа по компонентам  Х(О2)</param>
        /// <param name="xn2">состав газа по компонентам Х(N2)</param>
        /// <param name="xso2">состав газа по компонентам Х(SO2)</param>
        /// <param name="xno2">состав газа по компонентам Х(NO2)</param>
        /// <param name="xh2s">состав газа по компонентам Х(H2S)</param>
        /// <param name="xh2o">состав газа по компонентам Х(H2O)</param>
        /// <param name="m_co">Пересчет сухого газа на состав влажного газа</param>
        /// <param name="m_co2">Пересчет сухого газа на состав влажного газа</param>
        /// <param name="m_o2">Пересчет сухого газа на состав влажного газа</param>
        /// <param name="m_n2">Пересчет сухого газа на состав влажного газа</param>
        /// <param name="m_so2">Пересчет сухого газа на состав влажного газа</param>
        /// <param name="m_no2">Пересчет сухого газа на состав влажного газа</param>
        /// <param name="m_h2s">Пересчет сухого газа на состав влажного газа</param>
        /// <param name="m_h2o">Пересчет сухого газа на состав влажного газа</param>
        /// <returns></returns>
        public static double DINVZ(double k, double xco, double xco2, double xo2, double xn2, double xso2, double xno2, double xh2s, double xh2o,
            double m_co, double m_co2, double m_o2, double m_n2, double m_so2, double m_no2, double m_h2s, double m_h2o)
        {
            return Math.Round((k * xco * m_co) + (k * xco2 * m_co2) + (k * xo2 * m_o2) + (k * xn2 * m_n2) + (k * xso2 * m_so2) + (k * xno2 * m_no2) + (k * xh2s * m_h2s) + (k * xh2o * m_h2o), 8);
        }

        /// <summary>
        /// Расход влажного газа при рабочих условиях
        /// </summary>
        /// <param name="vnorm">объемный расход сухого газа при нормальных условиях</param>
        /// <param name="xg">влажность очищаемого газа</param>
        /// <param name="beta">Поправочный коэффициент BETA</param>
        /// <returns></returns>
        public static double VG(double vnorm, double xg, double beta)
        {
            return Math.Round(vnorm * beta * (1 + (xg / 0.804)), 2);
        }

        /// <summary>
        /// Общая площадь проходного сечения циклонов
        /// </summary>
        /// <param name="vg">Расход влажного газа при рабочих условиях</param>
        /// <param name="wopt">оптимальная скорость газа в циклоне</param>
        /// <returns></returns>
        public static double SCIK(double vg, double wopt)
        {
            return Math.Round(vg / wopt, 2);
        }

        /// <summary>
        /// Расчетный диаметр одиночного циклона
        /// </summary>
        /// <param name="nc">количество единичных циклонов в группе</param>
        /// <param name="scik">Общая площадь проходного сечения циклонов</param>
        /// <returns></returns>
        public static double DC(double nc, double scik)
        {
            //=10^(3)*(КОРЕНЬ(B39/(0,758*B25)))
            return Math.Round(Math.Pow(10, 3) * Math.Sqrt(scik / (0.758 * nc)));
        }

        /// <summary>
        /// Расчетная скорость газа в циклоне
        /// </summary>
        /// <param name="nc">количество единичных циклонов в группе</param>
        /// <param name="vg">Расход влажного газа при рабочих условиях</param>
        /// <param name="ds">Диаметр циклона из стандартного ряда</param>
        /// <returns></returns>
        public static double WG(double nc, double vg, double ds)
        {
            //=B38/(0,785*B25*10^(-6)*B41*B41)
            return Math.Round(vg / (0.785 * nc * Math.Pow(10, -6) * ds * ds), 3);
        }

        /// <summary>
        /// Отличие расчетной скорости от оптимальной
        /// </summary>
        /// <param name="wopt">оптимальная скорость газа в циклоне</param>
        /// <param name="wg">Расчетная скорость газа в циклоне</param>
        /// <returns></returns>
        public static double DW(double wopt, double wg)
        {
            return Math.Round((wg - wopt) * 100 / wopt, 2);
        }

        /// <summary>
        /// Коэффициент гидравлического сопротивления циклона
        /// </summary>
        /// <param name="kgc">коэффициент гидравлического сопротивления циклона </param>
        /// <param name="k1">коэффициент, учитывающий отличие диаметра циклона</param>
        /// <param name="k2">поправка на влияние запыленности газа</param>
        /// <param name="k3">поправка на влияние групповой компоновки циклонов</param>
        /// <returns></returns>
        public static double KSIC(double kgc, double k1, double k2, double k3)
        {
            return kgc * k1 * k2 + k3;
        }

        /// <summary>
        /// Гидравлическое сопротивление циклона
        /// </summary>
        /// <param name="wdrab">Плотность влажного газа при рабочих условиях</param>
        /// <param name="wg">Расчетная скорость газа в циклоне</param>
        /// <param name="ksic">Коэффициент гидравлического сопротивления циклона</param>
        /// <returns></returns>
        public static double DP(double wdrab, double wg, double ksic)
        {
            return Math.Round(ksic * wdrab * wg * wg / 2);
        }

        /// <summary>
        /// Коэффициент КР
        /// </summary>
        /// <param name="drab">Плотность сухого газа при рабочих условиях</param>
        /// <param name="dp">Гидравлическое сопротивление циклона</param>
        /// <returns></returns>
        public static double KP(double drab, double dp)
        {
            return Math.Round(dp / drab);
        }

        /// <summary>
        /// Минимальный размер частиц
        /// </summary>
        /// <param name="dm">средний медианный размер частиц пыли, теоретически улавливаемых на 50</param>
        /// <param name="wdrab">Плотность влажного газа при рабочих условиях</param>
        /// <param name="dinvz">Динамическая вязкость газа при рабочих условиях</param>
        /// <param name="ds">Диаметр циклона из стандартного ряда</param>
        /// <param name="wg">Расчетная скорость газа в циклоне</param>
        /// <returns></returns>
        public static double DMIN(double dm, double wdrab, double dinvz, double ds, double wg)
        {
            //=B24*34,97*(10^(3))*(КОРЕНЬ((10^(-3)*(B41*B37))/(B36*B42)))
            return Math.Round(dm * 34.97 * 1000 * Math.Sqrt(0.001 * ds * dinvz / (wdrab * wg)), 2);
        }

        public static void SOLVE()
        {
            /*------------------------------------------------------------------------------------------
                                    ТУТ РАССЧЕТЫ                        */
            KD = K(XG);
            BETAD = BETA(PB, PG, TG);
            DNORMD = DNORM(XCO, XCO2, XO2, XN2, XSO2, XNO2, XH2S);
            DRABD = DRAB(BETAD, DNORMD);
            WDRABD = WDRAB(XG, BETAD, DNORMD);
            //--------------------------------------------------------
            M_COD = M_CO(TG);
            M_CO2D = M_CO2(TG);
            M_O2D = M_O2(TG);
            M_N2D = M_N2(TG);
            M_SO2D = M_SO2(TG);
            M_NO2D = M_NO2(TG);
            M_H2SD = M_H2S(TG);
            M_H2OD = M_H2O(TG);
            //--------------------------------------------------------
            DINVZD = DINVZ(KD, XCO, XCO2, XO2,
                XN2, XSO2, XNO2, XH2S, XH2O,
                M_COD, M_CO2D, M_O2D, M_N2D, M_SO2D,
                M_NO2D, M_H2SD, M_H2OD);
            VGD = VG(VNORM, XG, BETAD);
            SCIKD = SCIK(VGD, WOPT);
            DCD = DC(NC, SCIKD);
            WGD = WG(NC, VGD, DSD);
            DWD = DW(WOPT, WGD);
            KSICD = KSIC(KGC, K1, K2, K3);
            DPD = DP(WDRABD, WGD, KSICD);
            KPD = KP(DRABD, DPD);
            DMIND = DMIN(DM, WDRABD, DINVZD, DSD, WGD);
        }
    }
}
