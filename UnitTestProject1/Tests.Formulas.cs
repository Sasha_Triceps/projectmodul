﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Dust.Tests
{
    [TestClass]
    public class TestFormulas
    {
        [TestMethod]
        public void TestBETA()
        {
            //arrange
            var pb = 101;
            var pg = 15;
            var tg = 130;
            var r = 0.777;
            //act
            var result = Dust.Core.Formulas.BETA(pb, pg, tg);
            //assert
            Assert.AreEqual(r, result, "Не правильно");
        }

        [TestMethod]
        public void TestDNORM()
        {
            //arrange
            var xco = 0;
            var xco2 = 0.13;
            var xo2 = 0;
            var xn2 = 0.79;
            var xso2 = 0;
            var xno2 = 0;
            var xh2s = 0.11;
            var r = 1.413;
            //act
            var result = Dust.Core.Formulas.DNORM(xco, xco2, xo2, xn2, xso2, xno2, xh2s);
            //assert
            Assert.AreEqual(r, result, "Не правильно");
        }

        [TestMethod]
        public void TestDRAB()
        {
            //arrange
            var beta = 0.777;
            var dnorm = 1.413;
            var r = 1.098;
            //act
            var result = Dust.Core.Formulas.DRAB(beta, dnorm);
            //assert
            Assert.AreEqual(r, result, "Не правильно");
        }

        [TestMethod]
        public void TestWDRAB()
        {
            //arrange
            var xg = 0.013;
            var beta = 0.777;
            var dnorm = 1.413;
            var r = 1.09;
            //act
            var result = Dust.Core.Formulas.WDRAB(xg, beta, dnorm);
            //assert
            Assert.AreEqual(r, result, "Не правильно");
        }

        [TestMethod]
        public void TestK()
        {
            //arrange
            var xg = 0.013;
            var r = 0.984;
            //act
            var result = Dust.Core.Formulas.K(xg);
            //assert
            Assert.AreEqual(r, result, "Не правильно");
        }

        [TestMethod]
        public void TestM_CO()
        {
            //arrange
            var tg = 130;
            var r = 2.208E-05;
            //act
            var result = Dust.Core.Formulas.M_CO(tg);
            //assert
            Assert.AreEqual(r, result, "Не правильно");
        }

        [TestMethod]
        public void TestM_CO2()
        {
            //arrange
            var tg = 130;
            var r = 2.153E-05;
            //act
            var result = Dust.Core.Formulas.M_CO2(tg);
            //assert
            Assert.AreEqual(r, result, "Не правильно");
        }

        [TestMethod]
        public void TestM_O2()
        {
            //arrange
            var tg = 130;
            var r = 2.768E-05;
            //act
            var result = Dust.Core.Formulas.M_O2(tg);
            //assert
            Assert.AreEqual(r, result, "Не правильно");
        }

        [TestMethod]
        public void TestM_N2()
        {
            //arrange
            var tg = 130;
            var r = 2.282E-05;
            //act
            var result = Dust.Core.Formulas.M_N2(tg);
            //assert
            Assert.AreEqual(r, result, "Не правильно");
        }

        [TestMethod]
        public void TestM_SO2()
        {
            //arrange
            var tg = 130;
            var r = 1.757E-05;
            //act
            var result = Dust.Core.Formulas.M_SO2(tg);
            //assert
            Assert.AreEqual(r, result, "Не правильно");
        }

        [TestMethod]
        public void TestM_NO2()
        {
            //arrange
            var tg = 130;
            var r = 4.035E-05;
            //act
            var result = Dust.Core.Formulas.M_NO2(tg);
            //assert
            Assert.AreEqual(r, result, "Не правильно");
        }

        [TestMethod]
        public void TestM_H2S()
        {
            //arrange
            var tg = 130;
            var r = 2.707E-05;
            //act
            var result = Dust.Core.Formulas.M_H2S(tg);
            //assert
            Assert.AreEqual(r, result, "Не правильно");
        }

        [TestMethod]
        public void TestM_H2O()
        {
            //arrange
            var tg = 130;
            var r = 1.623E-06;
            //act
            var result = Dust.Core.Formulas.M_H2O(tg);
            //assert
            Assert.AreEqual(r, result, "Не правильно");
        }

        [TestMethod]
        public void TestM_DINVZ()
        {
            //arrange
            var k = 0.984;
            var xco = 0;
            var xco2 = 0.13;
            var xo2 = 0;
            var xn2 = 0.79;
            var xso2 = 0;
            var xno2 = 0;
            var xh2s = 0.11;
            var xh2o = 15;
            var m_co = 2.208E-05;
            var m_co2 = 2.153E-05;
            var m_o2 = 2.768E-05;
            var m_n2 = 2.282E-05;
            var m_so2 = 1.757E-05;
            var m_no2 = 4.035E-05;
            var m_h2s = 2.707E-05;
            var m_h2o = 1.623E-06;
            var r = 4.738E-05;
            //act
            var result = Dust.Core.Formulas.DINVZ(k, xco, xco2, xo2, xn2, xso2, xno2, xh2s, xh2o, m_co, m_co2, m_o2, m_n2, m_so2, m_no2, m_h2s, m_h2o);
            //assert
            Assert.AreEqual(r, result, "Не правильно");
        }

        [TestMethod]
        public void TestM_VG()
        {
            //arrange
            var vnorm = 16;
            var xg = 0.013;
            var beta = 0.777;
            var r = 12.64;
            //act
            var result = Dust.Core.Formulas.VG(vnorm, xg, beta);
            //assert
            Assert.AreEqual(r, result, 0.01, "Не правильно");
        }

        [TestMethod]
        public void TestM_SCIK()
        {
            //arrange
            var vg = 12.63;
            var wopt = 1.7;
            var r = 7.43;
            //act
            var result = Dust.Core.Formulas.SCIK(vg, wopt);
            //assert
            Assert.AreEqual(r, result, "Не правильно");
        }

        [TestMethod]
        public void TestM_DC()
        {
            //arrange
            var nc = 6;
            var scik = 7.43;
            var r = 1278;
            //act
            var result = Dust.Core.Formulas.DC(nc, scik);
            //assert
            Assert.AreEqual(r, result, "Не правильно");
        }

        [TestMethod]
        public void TestWG()
        {
            //arrange
            var nc = 6;
            var vg = 12.63;
            var ds = 1200;
            var r = 1.863;
            //act
            var result = Dust.Core.Formulas.WG(nc, vg, ds);
            //assert
            Assert.AreEqual(r, result, 0.001, "Не правильно");
        }

        [TestMethod]
        public void TestDW()
        {
            //arrange
            var wopt = 1.7;
            var wg = 1.863;
            var r = 9.60;
            //act
            var result = Dust.Core.Formulas.DW(wopt, wg);
            //assert
            Assert.AreEqual(r, result, 0.01, "Не правильно");
        }

        [TestMethod]
        public void TestKSIC()
        {
            //arrange
            var kgc = 1050;
            var k1 = 1;
            var k2 = 0.93;
            var k3 = 60;
            var r = 1036.5;
            //act
            var result = Dust.Core.Formulas.KSIC(kgc, k1, k2, k3);
            //assert
            Assert.AreEqual(r, result, "Не правильно");
        }

        [TestMethod]
        public void TestDP()
        {
            //arrange
            var wdrab = 1.091;
            var wg = 1.863;
            var ksic = 1036.5;
            var r = 1962;
            //act
            var result = Dust.Core.Formulas.DP(wdrab, wg, ksic);
            //assert
            Assert.AreEqual(r, result, "Не правильно");
        }

        [TestMethod]
        public void TestKP()
        {
            //arrange
            var drab = 1.098;
            var dp = 1962;
            var r = 1787;
            //act
            var result = Dust.Core.Formulas.KP(drab, dp);
            //assert
            Assert.AreEqual(r, result, "Не правильно");
        }

        [TestMethod]
        public void TestDMIN()
        {
            //arrange
            var dm = 1.94;
            var wdrab = 1.091;
            var dinvz = 4.738E-05;
            var ds = 1200;
            var wg = 1.863;
            var r = 358.85;
            //act
            var result = Dust.Core.Formulas.DMIN(dm, wdrab, dinvz, ds, wg);
            //assert
            Assert.AreEqual(r, result, 0.05, "Не правильно");
        }
    }
}
