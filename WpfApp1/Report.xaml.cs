﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Reporting.WinForms;
using System.Data;
using System.Windows.Forms;
using Dust.Core;

namespace WpfApp1
{
    /// <summary>
    /// Логика взаимодействия для Report.xaml
    /// </summary>
    public partial class Report : Window

    {
        List<ForReport> rListforReport;
        public Report(List<ForReport> rListforReport)
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            this.rListforReport = rListforReport;
            ReportView.RefreshReport();
            ReportView.Reset();
            DataTable dt = new DataTable();
            DataColumn colDate1 = new DataColumn("name", typeof(string));
            dt.Columns.Add(colDate1);
            DataColumn colDate2 = new DataColumn("value", typeof(string));
            dt.Columns.Add(colDate2);
            foreach (var a in rListforReport)
            {
                dt.Rows.Add(a.name, a.value);
            }
            ReportDataSource ds = new ReportDataSource("DataSet1", dt);
            ReportView.LocalReport.DataSources.Add(ds);
            ReportView.LocalReport.ReportEmbeddedResource = "WpfApp1.Report.rdlc";
            ReportView.RefreshReport();
            
        }

    }
}
