﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Dust.Core;

namespace WpfApp1
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<ForReport> rListforReport = new List<ForReport>();
        
        public MainWindow()
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            ReportButton.Click += (s, e) =>
            {
                Report WindowReport = new Report(rListforReport);
                WindowReport.ShowDialog();
            };
        }


        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Formulas.PB = Convert.ToDouble(textBox_PB.Text);
                Formulas.VNORM = Convert.ToDouble(textBox_VNORM.Text);
                Formulas.XG = Convert.ToDouble(textBox_XG.Text);
                Formulas.PG = Convert.ToDouble(textBox_PG.Text);
                Formulas.XCO = Convert.ToDouble(textBox_XCO.Text);
                Formulas.XCO2 = Convert.ToDouble(textBox_XCO2.Text);
                Formulas.XO2 = Convert.ToDouble(textBox_XO2.Text);
                Formulas.XN2 = Convert.ToDouble(textBox_XN2.Text);
                Formulas.XSO2 = Convert.ToDouble(textBox_XSO2.Text);
                Formulas.XNO2 = Convert.ToDouble(textBox_XNO2.Text);
                Formulas.XH2S = Convert.ToDouble(textBox_XH2S.Text);
                Formulas.XH2O = Convert.ToDouble(textBox_XH2O.Text);
                Formulas.TG = Convert.ToDouble(textBox_TG.Text);
                Formulas.ZG = Convert.ToDouble(textBox_ZG.Text);
                Formulas.N = Convert.ToDouble(textBox_N.Text);
                Formulas.DI = Convert.ToDouble(textBox_DI.Text);
                Formulas.DPART = Convert.ToDouble(textBox_DPART.Text);
                Formulas.CLG = Convert.ToDouble(textBox_CLG.Text);
                Formulas.WOPT = Convert.ToDouble(textBox_WOPT.Text);
                Formulas.DM = Convert.ToDouble(textBox_DM.Text);
                Formulas.NC = Convert.ToDouble(textBox_NC.Text);
                Formulas.KGC = Convert.ToDouble(textBox_KGC.Text);
                Formulas.K1 = Convert.ToDouble(textBox_K1.Text);
                Formulas.K2 = Convert.ToDouble(textBox_K2.Text);
                Formulas.K3 = Convert.ToDouble(textBox_K3.Text);
                Formulas.DSD = Convert.ToDouble(textBox_DSD.Text);
            }
            catch (FormatException)
            { MessageBox.Show("Неверно введено число."); }
            /*ТУТ РАССЧЕТЫ*/
            Formulas.SOLVE();

            textBox_BETA.Text = Convert.ToString(Formulas.BETAD);
            textBox_DNORM.Text = Convert.ToString(Formulas.DNORMD);
            textBox_DRAB.Text = Convert.ToString(Formulas.DRABD);
            textBox_WDRAB.Text = Convert.ToString(Formulas.WDRABD);
            textBox_DINVZ.Text = Convert.ToString(Formulas.DINVZD);
            textBox_VG.Text = Convert.ToString(Formulas.VGD);
            textBox_SCIK.Text = Convert.ToString(Formulas.SCIKD);
            textBox_DC.Text = Convert.ToString(Formulas.DCD);
            textBox_WG.Text = Convert.ToString(Formulas.WGD);
            textBox_DW.Text = Convert.ToString(Formulas.DWD);
            textBox_KSIC.Text = Convert.ToString(Formulas.KSICD);
            textBox_DP.Text = Convert.ToString(Formulas.DPD);
            textBox_KP.Text = Convert.ToString(Formulas.KPD);
            textBox_DMIN.Text = Convert.ToString(Formulas.DMIND);

            rListforReport.Add(new ForReport { name = "Поправочный коэффициент BETA", value = textBox_BETA.Text });
            rListforReport.Add(new ForReport { name = "Плотность сухого газа при нормальных условиях, кг/м3", value = textBox_DNORM.Text });
            rListforReport.Add(new ForReport { name = "Плотность сухого газа при рабочих условиях, кг/м3", value = textBox_DRAB.Text });
            rListforReport.Add(new ForReport { name = "Плотность влажного газа при рабочих условиях, кг/м3", value = textBox_WDRAB.Text });
            rListforReport.Add(new ForReport { name = "Динамическая вязкость газа при рабочих условиях", value = textBox_DINVZ.Text });
            rListforReport.Add(new ForReport { name = "Расход влажного газа при рабочих условиях, м3/c", value = textBox_VG.Text });
            rListforReport.Add(new ForReport { name = "Общая площадь проходного сечения циклонов, м2", value = textBox_SCIK.Text });
            rListforReport.Add(new ForReport { name = "Расчётный диаметр одиночного циклона, мм", value = textBox_DC.Text });
            rListforReport.Add(new ForReport { name = "Расчётная скорость газа в циклоне, м/c", value = textBox_WG.Text });
            rListforReport.Add(new ForReport { name = "Отличие расчётной скорости от оптимальной, %", value = textBox_DW.Text });
            rListforReport.Add(new ForReport { name = "Коэффициент гидравлического сопротивления циклона, Па", value = textBox_KSIC.Text });
            rListforReport.Add(new ForReport { name = "Гидравлическое сопротивление циклона, Па", value = textBox_DP.Text });
            rListforReport.Add(new ForReport { name = "Коэффициент КР", value = textBox_KP.Text });
            rListforReport.Add(new ForReport { name = "Минимальный размер частиц, мкм", value = textBox_DMIN.Text });

        }
    }
}
